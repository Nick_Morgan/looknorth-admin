import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Header from '../components/Header';
import Dashboard from '../components/Dashboard';
import UserTable from '../components/UserTable';

const AppRouter = () => (
  <BrowserRouter>
    <div style={{height: '100%'}}>
      <Header>
        <Switch>
          <Route path="/" component={Dashboard} exact={true}></Route>
          <Route path="/UserTable" component={UserTable} exact={true}></Route>
        </Switch>
      </Header>
    </div>
  </BrowserRouter>
);

export default AppRouter;
