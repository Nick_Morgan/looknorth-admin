import {
  AUDIENCE,
  AUTH0_ACCESS_TOKEN_URL,
  AUTH0_USERS_URL,
  CLIENT_ID,
  AUTH0_UPDATE_USERS_URL,
  CLIENT_SECRET,
  GRANT_TYPE
} from '../config/constants';
import fetchPonyfill from 'fetch-ponyfill';

const { fetch, Headers } = fetchPonyfill()

export const fetchAccessToken = async () => {
  const options = {
    method: 'POST',
    body: JSON.stringify({
      audience: AUDIENCE,
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
      grant_type: GRANT_TYPE
    }),
    headers: new Headers({ 'Content-Type': 'application/json' })
  }
  const response = await fetch(AUTH0_ACCESS_TOKEN_URL, options)
  const json = await response.json()

  return json.access_token
}

export const fetchUsers = async () => {
  const accessToken =  await fetchAccessToken()
  const authorization = `Bearer ${accessToken}`
  const response = await fetch(AUTH0_USERS_URL,
    { headers: new Headers({ authorization }) }
  )
  const json = await response.json()

  return json
}

export const createUsers = async (user) => {
  const accessToken =  await fetchAccessToken()
  const authorization = `Bearer ${accessToken}`
  const options2 = {
    method: 'POST',
    body: JSON.stringify({ //put user data here with JSON.stringify
      user_id: "test",
      connection: "Username-Password-Authentication",
      email: "john.doe@gmail.com",
      password: "secret",
      user_metadata: {},
      email_verified: false,
      verify_email: false,
      app_metadata: {}
    }),
    headers: new Headers({ 'Content-Type': 'application/json', authorization })
  }
  const response = await fetch(AUTH0_USERS_URL, options2)
  const json = await response.json()

  return json
}

export const updateUsers = async (id,user) => {
  const id1 = id; //user id needs to be passed in with a '/'
  const accessToken =  await fetchAccessToken()
  const authorization = `Bearer ${accessToken}`
  const options2 = {
    method: 'PATCH',
    body: JSON.stringify(
      user
    ),
    headers: new Headers({ 'Content-Type': 'application/json', authorization })
  }
  const response = await fetch(AUTH0_UPDATE_USERS_URL+id1, options2)
  const json = await response.json()

  return json
}

export const deleteUsers = async (id) => {
  const id2 = "/auth0|test"; //user id needs to be passed in with a '/'
  const accessToken =  await fetchAccessToken()
  const authorization = `Bearer ${accessToken}`
  const options2 = {
    method: 'DELETE',
    headers: new Headers({ 'Content-Type': 'application/json', authorization })
  }
  const response = await fetch(AUTH0_USERS_URL+id2, options2)
  const json = await response.json()

  return json
}
